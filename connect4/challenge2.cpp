#include <iostream>
using namespace std;

void intInput(int &pointer);// Filter Input 

const int columns=7,row=6;


void intInput(int &pointer){
		
	int tmp=0;
	
	while(tmp<=0){
		
		cin >> tmp;
			
		if(cin.fail()){
			
			cin.clear();
			cin.ignore(10000, '\n');
	
		}else if((tmp>0) && (tmp <8)){ // check input between 1 -7
			pointer=tmp-1;
			break;
		}else{
			tmp=-1;
		}	
		
		cout << "Invalid Input :";	
		
	}
		
}

int Draw(int array[columns][row]){
	
	int i,j,status=2;// 0 => conitinue // 1 => winner // 2 => Draw
	int score[4][2][13]={0};
	/*
		score[0] vertically
		score[1] horizontally
		score[2] diagonally	\
		score[3] diagonally	/	
	*/
	cout << endl << endl << endl;
	cout << "\t\t\t";
	for (int j=0;j<columns;j++)cout << " " << (j+1) << " ";	
	cout << endl;
	for (i=0;i<row;i++){
		
		cout << "\t\t\t";
		
		for (j=0;j<columns;j++){
			
			if(array[j][i] == 0){
				if(status!=1)	status=0;
				cout << "[ ]";
				
				score[1][0][i]=0;	
				score[1][1][i]=0;
				
				score[2][0][(6-j+i)]=0;
				score[2][1][(6-j+i)]=0;
				
				score[3][0][(j+i)]=0;
				score[3][1][(j+i)]=0;
				
			}else{
				
				if(array[j][i]==1){
						
					score[1][0][i]++;					
					score[1][1][i]=0;
					
					score[0][0][j]++;					
					score[0][1][j]=0;					

					score[2][0][(6-j+i)]++;					
					score[2][1][(6-j+i)]=0;

					score[3][0][(j+i)]++;					
					score[3][1][(j+i)]=0;
				
					cout << "[o]";
					
				}else{

					score[1][0][i]=0;
					score[1][1][i]++;
					
					score[0][0][j]=0;
					score[0][1][j]++;											

					score[2][0][(6-j+i)]=0;				
					score[2][1][(6-j+i)]++;

					score[3][0][(j+i)]=0;					
					score[3][1][(j+i)]++;	

					cout << "[x]";
																								
				}
			
			}
			
			if(score[0][0][j]==4)status=1; // Player 1 vertically check
			if(score[0][1][j]==4)status=1; // Player 2 vertically check

			if(score[1][0][i]==4)status=1; // Player 1 horizontally check
			if(score[1][1][i]==4)status=1; // Player 2 horizontally check

			if(score[2][0][(6-j+i)]==4)status=1; // Player 1 diagonally[/] check	
			if(score[2][1][(6-j+i)]==4)status=1; // Player 2 diagonally[/] check
			
			if(score[3][0][i+j]==4)status=1; // Player 1 diagonally[\] check
			if(score[3][1][i+j]==4)status=1; // Player 2 diagonally[\] check		
		
		}
		
		
		cout << endl;
		
	}	

	return status;

}

int main() {

	int key,player=0,ins,result;
	int board[columns][row]={0};// [column][row]
	
	bool run=true;
	
	Draw(board);
	
	while (run==true){
			
		player=(player%2);
		
		cout << "\t\t\t     Player " << player+1 << " : ";
		
		intInput(key);
		
		for (int i=0;i<=row;i++){
			
			if(i==row){
				system("cls");
				Draw(board);
				cout << "\t\t\t Column is not empty" << endl;				
				break;
			}
			
			ins=row-(i+1);
				
			if(board[key][ins] == 0){
									
				system("cls");
					
				board[key][ins]=player+1;
				
				result=Draw(board);
				
				if(result == 2 ){
					run=false;
					cout << "Draw!";
				}else if(result == 1){
					run=false;	
					cout << "\t\t\t     Player " << player+1 << " Win ";	
				}
					
				player++;			
				break;
			}
			
		}
		
	

	}

}
